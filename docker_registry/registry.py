"""Docker Registry handler"""
import logging
import json
from datetime import datetime, timedelta
import requests

logging = logging.getLogger(__name__) # pylint: disable=invalid-name

class Registry(object):
    def __init__(self, registry, mock=False):
        "Set mock to prevent any write operations"
        self.registry = registry
        self.mock = mock

    def gettagage(self, repo, tag):
        "Get the creation datestamp of a tag on a repo"
        res = requests.get("{}/v2/{}/manifests/{}".format(self.registry, repo, tag))
        if res.status_code != 200:
            message = "Unexpected HTTP response {} getting age for {}:{}".format(res.status_code, repo, tag)
            logging.error(message)
            raise Exception(message)
        tagobj = res.json()
        if 'history' not in tagobj or len(tagobj['history']) < 1:
            message = "Tag {}:{} has no history".format(repo, tag)
            logging.warning(message)
            return None
        histobj = json.loads(tagobj['history'][0]['v1Compatibility'])
        datestr = histobj['created'][:19]
        datestamp = datetime.strptime(datestr, '%Y-%m-%dT%H:%M:%S')
        return datestamp

    def getrepoage(self, repo):
        age = datetime.min
        tags = self.gettags(repo)
        for tag in tags:
            tagage = self.gettagage(repo, tag)
            if tagage is not None and tagage > age:
                age = tagage
        # If we don't have an age, return None
        if age == datetime.min:
            return None
        return age

    def getcontentdigest(self, repo, tag):
        """Retrieve the content ID for a tag on a repository"""
        r = requests.get("{}/v2/{}/manifests/{}".format(self.registry, repo, tag),
            headers={'Accept': 'application/vnd.docker.distribution.manifest.v2+json'})
        if r.status_code != 200:
            message = "Unexpected HTTP response {} getting content ID for {}:{}".format(r.status_code, repo, tag)
            logging.error(message)
            raise Exception(message)
        return r.headers['docker-content-digest']

    def gettags(self, repo):
        r = requests.get("{}/v2/{}/tags/list".format(self.registry, repo))
        if r.status_code != 200:
            message = "Unexpected HTTP response {} listing tags for {}".format(r.status_code, repo)
            logging.error(message)
            raise Exception(message)
        tags = r.json()['tags']
        if tags is None:
            logging.warning("Repo {} has no tags".format(repo))
            return []
        return tags

    def getprunelist(self, repo, keep=10):
        hashes = {}

        tags = self.gettags(repo)
        if tags == []:
            return {}

        branch = repo.split('/', 1)[-1]
        if branch.startswith("feature") or branch.startswith("hotfix"):
            age = self.getrepoage(repo)
            if age is not None and age < datetime.utcnow() - timedelta(28):
                for tag in tags:
                    hash = self.getcontentdigest(repo, tag)
                    if hash not in hashes:
                        hashes[hash] = [tag]
                    else:
                        hashes[hash].append(tag)
                return hashes

        keephashes = set()
        for tag in tags[:]:
            if not tag.isdigit():
                logging.debug("Excluding {}:{} from prune".format(repo, tag))
                tags.remove(tag)

        # Convert tags to integers, sort them and convert them back
        tags = [str(i) for i in sorted([int(s) for s in tags], reverse=True)]
        for tag in tags[:keep]:
            keephashes.add(self.getcontentdigest(repo, tag))
        tags = tags[keep:]
        for tag in tags:
            hash = self.getcontentdigest(repo, tag)
            if hash in keephashes:
                message = "Skipping {}:{}: hash matches kept image".format(repo,tag)
                logging.info(message)
                continue
            if hash not in hashes:
                hashes[hash] = []
            hashes[hash].append(tag)

        return hashes

    def catalog(self):
        return requests.get("{}/v2/_catalog".format(self.registry)).json()['repositories']

    def delete(self, repo, tag):
        hash = self.getcontentdigest(repo, tag)
        logging.info("Deleting {} ({}:{}){}".format(hash, repo, tag, ' (MOCK MODE)' if self.mock else ''))
        if not self.mock:
            r = requests.delete("{}/v2/{}/manifests/{}".format(self.registry, repo, hash))
            if r.status_code != 202:
                message = "Unexpected HTTP response {} deleting {} ({}:{})".format(r.status_code, hash, repo, tag)
                logging.error(message)

    def prune(self, repo):
        hashes = self.getprunelist(repo)
        for hash in hashes:
            tags = ', '.join("{}:{}".format(repo, tag) for tag in hashes[hash])
            logging.info("Deleting {} ({}){}".format(hash, tags, ' (MOCK MODE)' if self.mock else ''))
            if not self.mock:
                r = requests.delete("{}/v2/{}/manifests/{}".format(self.registry, repo, hash))
                if r.status_code != 202:
                    message = "Unexpected HTTP response {} deleting {} ({})".format(r.status_code, hash, tags)
                    logging.error(message)
