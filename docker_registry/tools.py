from .registry import Registry
import logging
import sys
import argparse

# Skru av logger fra requests
def listregistry():
    parser = argparse.ArgumentParser(description="""\
List images and tags in Docker registry.
""")
    parser.add_argument('registry', help="Docker registry URL")
    args = parser.parse_args()
    r = Registry(args.registry)
    for repo in r.catalog():
        print(repo)
        for tag in r.gettags(repo):
            print(" -", tag)

def deletetag():
    parser = argparse.ArgumentParser(description="""\
Delete an image from a docker registry.
""")
    parser.add_argument('registry', help="Docker registry URL")
    parser.add_argument('repotag', help="Repo and tag to delete, in the format repo:tag")
    parser.add_argument('-m', '--mock', action='store_true',
            help="Run in mock mode (don't really delete)")
    parser.add_argument('-d', '--debug', action='store_true',
            help="Enable debug logging")
    args = parser.parse_args()

    logging.getLogger("requests").setLevel(logging.WARNING)
    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    r = Registry(args.registry, mock=args.mock)
    (repo, tag) = args.repotag.split(':')
    if repo == '' or tag == '':
        logging.error("Can't use repo name '%s': must be of the format 'repo:tag'", args.repotag)
        sys.exit(1)

    r.delete(repo, tag)

def pruneregistry():
    parser = argparse.ArgumentParser(description="""\
Prune images from Docker registry. Trim more than 10 numbered tags \
from each repo and delete old feature/hotfix repos.\
""")
    parser.add_argument('registry', help="Docker registry URL")
    parser.add_argument('-m', '--mock', action='store_true',
            help="Run in mock mode (don't really delete)")
    parser.add_argument('-d', '--debug', action='store_true',
            help="Enable debug logging")
    args = parser.parse_args()

    logging.getLogger("requests").setLevel(logging.WARNING)
    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    r = Registry(args.registry, mock=args.mock)
    for repo in r.catalog():
        r.prune(repo)

if __name__=='__main__':
    listregistry()
