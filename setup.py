from setuptools import setup, find_packages

setup(
    name="dockerregistry",
    version="1.2.0",
    author="James McDonald",
    author_email="james@jamesmcdonald.com",
    packages=find_packages(),
    install_requires=[
        'requests',
        'setuptools',
    ],
    entry_points={
        'console_scripts': [
            'dr_prune = docker_registry.tools:pruneregistry',
            'dr_list = docker_registry.tools:listregistry',
            'dr_delete = docker_registry.tools:deletetag',
        ],
    }
)
